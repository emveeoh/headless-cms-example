import { loadAsyncComponents } from '@akryum/vue-cli-plugin-ssr/client'
import routes from 'vue-auto-routing'
import { createRouterLayout } from 'vue-router-layout'

import { createApp } from './main'

const RouterLayout = createRouterLayout(layout => {
  return import('@/layouts/' + layout + '.vue')
})

createApp({
  async beforeApp({ router }) {
    router.addRoutes([
      {
        path: '/',
        component: RouterLayout,
        children: routes,
      },
    ])
    await loadAsyncComponents({ router })
  },

  afterApp({ app, router }) {
    router.onReady(() => {
      app.$mount('#app')
    })
  },
})
